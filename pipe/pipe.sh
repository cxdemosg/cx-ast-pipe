#!/usr/bin/env bash
#
# Execute CxAST scan
#

source "$(dirname "$0")/common.sh"

info "Executing cx-ast-pipe..."

# Required globals:
#   AST_TENANT
#   AST_CLIENT_ID
#   AST_CLIENT_SECRET
#
# Optional globals:
#   AST_BASE_URI
#   AST_AGENT
#   AST_SCANTYPE
#   SAST_SCAN_PRESET
#   OUTPUT_PATH
#   REPORT_FORMAT
#   PARAMS

# Required parameters
AST_TENANT=${AST_TENANT:?'AST_TENANT variable missing'}
AST_CLIENT_ID=${AST_CLIENT_ID:?'AST_TENANT variable missing'}
AST_CLIENT_SECRET=${AST_CLIENT_SECRET:?'AST_CLIENT_SECRET variable missing'}

# Default parameters

AST_BASE_URI=${AST_BASE_URI:="https://eu.ast.checkmarx.net/"}
AST_AGENT=${AST_AGENT:="MyASTCLI"}
AST_SCANTYPE=${AST_SCANTYPE:="sast,sca,kics"}
SAST_SCAN_PRESET=${SAST_SCAN_PRESET:="Checkmarx Default"}
OUTPUT_NAME=${OUTPUT_NAME:="cx_result"}
REPORT_FORMAT=${REPORT_FORMAT:="sarif"}
PARAMS=${PARAMS:=""}

/app/bin/cx scan create \
  --agent "${AST_AGENT}" \
  --base-uri "${AST_BASE_URI}" \
  --client-id "${AST_CLIENT_ID}" \
  --client-secret "${AST_CLIENT_SECRET}" \
  --tenant "${AST_TENANT}" \
  --project-name "${BITBUCKET_REPO_SLUG}" \
  --branch "${BITBUCKET_BRANCH}" \
  --scan-types "${AST_SCANTYPE}" \
  --sast-preset-name "${SAST_SCAN_PRESET}" \
  --output-name " ${OUTPUT_NAME}" \
  --output-path "${BITBUCKET_CLONE_DIR}/reports" \
  --report-format "${REPORT_FORMAT}" \
  --file-source "${BITBUCKET_CLONE_DIR}" \
  ${PARAMS}

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi