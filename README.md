# Bitbucket Pipelines Pipe: cx-ast-pipe

This pipe enables executing CxAST scan with Bitbucket pipelines, and is extended based on CxAST CLI.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: docker://cxdemosg/cx-ast-pipe:latest
    variables:
      AST_TENANT: $AST_TENANT
      AST_CLIENT_ID: $AST_CLIENT_ID
      AST_CLIENT_SECRET: $AST_CLIENT_SECRET
      # VARNAME: "<string>"
  
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AST_TENANT (*) | CxAST tenant name, recommended to use secured repository variable |
| AST_CLIENT_ID (*) | CxAST OAuth client id, recommended to use securedrepository variable |
| AST_CLIENT_SECRET (*) | CxAST OAuth client secret, recommended to use secured repository variable |
| AST_BASE_URI | CxAST URI. Default: `https://eu.ast.checkmarx.net/`|
| AST_AGENT | CxAST agent name. Default: `MyASTCLI` |
| AST_SCANTYPE | Selected CxAST scanners. Default `sast,sca,kics` |
| SAST_SCAN_PRESET | Selected SAST scan preset. Default: `Checkmarx Default`|
| OUTPUT_NAME | Name of saved report. Default: `cx_result`|
| REPORT_FORMAT | Report format. Default: `sarif`|
| PARAMS | Additional CxAST Parameters.  Default: `` |

_(*) = required variable._

## Details
This bitbucket pipe is extended from CxAST CLI [[1]], you can refer to CxAST knowledge center [[2]] on usage.

## Examples

Example with artifact download:

```yaml
script:
  - pipe: docker://cxdemosg/cx-ast-pipe:latest
    variables:
      AST_TENANT: $AST_TENANT
      AST_CLIENT_ID: $AST_CLIENT_ID
      AST_CLIENT_SECRET: $AST_CLIENT_SECRET
artifacts:
  - reports/**
```

## Support

AST-CLI [[1]]  
CxAST CLI KC [[2]]  

[1]: https://github.com/Checkmarx/ast-cli "AST-CLI"
[2]: https://checkmarx.atlassian.net/wiki/spaces/AST/pages/2445443121/CLI+Tool "CxAST CLI KC"
